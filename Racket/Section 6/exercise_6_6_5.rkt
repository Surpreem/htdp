;; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-beginner-reader.ss" "lang")((modname exercise_6_6_5) (read-case-sensitive #t) (teachpacks ((lib "convert.ss" "teachpack" "htdp") (lib "guess.ss" "teachpack" "htdp") (lib "master.ss" "teachpack" "htdp") (lib "draw.ss" "teachpack" "htdp"))) (htdp-settings #(#t constructor repeating-decimal #f #t none #f ((lib "convert.ss" "teachpack" "htdp") (lib "guess.ss" "teachpack" "htdp") (lib "master.ss" "teachpack" "htdp") (lib "draw.ss" "teachpack" "htdp")))))
;; A circle is a structure
;;   (make-circle center radius color)
;; where center is a posn structure and radius is a number
;; and color is a symbol.
(define-struct circle (center radius color))

;; fun-for-circle : circle -> ???
;; (define (fun-for-circle a-circle)
;;   ... (circle-center a-circle) ...
;;   ... (circlr-radius a-circle) ...
;;   ... (circle-color a-circle) ...)

;; clear-a-circle : circle -> true
;; to clear the corresponding circle on the canvas
(define (clear-a-circle circle)
  (clear-circle (circle-center circle)
                (circle-radius circle)
                (circle-color circle)))


;; draw-a-circle : circle -> true
;; to draw a colcored circle
(define (draw-a-circle circle)
  (draw-circle (circle-center circle)
               (circle-radius circle)
               (circle-color circle)))

(start 300 300)
(define circle1 (make-circle (make-posn 100 100) 50 'red))
(draw-a-circle circle1)
(clear-a-circle circle1)
