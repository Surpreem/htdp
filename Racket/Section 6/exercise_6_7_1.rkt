;; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-beginner-reader.ss" "lang")((modname exercise_6_7_1) (read-case-sensitive #t) (teachpacks ((lib "convert.ss" "teachpack" "htdp") (lib "guess.ss" "teachpack" "htdp") (lib "master.ss" "teachpack" "htdp") (lib "draw.ss" "teachpack" "htdp"))) (htdp-settings #(#t constructor repeating-decimal #f #t none #f ((lib "convert.ss" "teachpack" "htdp") (lib "guess.ss" "teachpack" "htdp") (lib "master.ss" "teachpack" "htdp") (lib "draw.ss" "teachpack" "htdp")))))
;; A gallows is a structure
;;   (make-gallows start width length color)
;; where start is a posn structure and width, length are numbers
;; and color is a symbol.
(define-struct gallows (start width length color))

;; A circle is a structure
;;   (make-circle center radius color)
;; where center is a posn structure and raius is a number and color is a symbol.
(define-struct circle (center radius color))

;; A body is a structure
;;   (make-body start length color)
;; where start is a posn structure and length is a number and color is a symbol.
(define-struct body (start length color))

;; A arm&leg is a structure
;;   (make-arm&leg start length angle color)
;; where start is a posn structure and length, angle is a number
;; and color is a symbol.
(define-struct arm&leg (start length angle color))


;; tanslate-point-x : posn delta -> posn
;; to translate the point by delta pixels on the x-axis
(define (translate-point-x pt delta)
  (make-posn (+ (posn-x pt) delta)
             (posn-y pt)))

;; tanslate-point-y : posn delta -> posn
;; to translate the point by delta pixels on the y-axis
(define (translate-point-y pt delta)
  (make-posn (posn-x pt)
             (+ (posn-y pt) delta)))

;; translate-point : posn posn -> posn
;; to translate the point by delta pixels on both x-axis and y-axis
(define (translate-point pt delta)
  (make-posn (+ (posn-x pt) (posn-x delta))
             (+ (posn-y pt) (posn-y delta))))

;; PI
(define PI 3.141592)

;; convert-deg2rad : number -> number
;; to convert degree to radian
(define (convert-deg2rad degree)
  (/ (* PI degree) 180))

   
;; draw-a-gallows : gallows -> boolean
;; to draw a gallows
(define (draw-a-gallows gallows)
  (and (draw-solid-line (gallows-start gallows)
                        (translate-point-x (gallows-start gallows)
                                           (gallows-width gallows))
                        (gallows-color gallows))
       (draw-solid-line (translate-point-x (gallows-start gallows)
                                           (gallows-width gallows))
                        (translate-point-y (translate-point-x (gallows-start gallows)
                                                              (gallows-width gallows))
                                           (gallows-length gallows))
                        (gallows-color gallows))))

;; draw-a-noose : circle -> boolean
;; to draw a noose
(define (draw-a-noose circle)
  (draw-circle (circle-center circle)
               (circle-radius circle)
               (circle-color circle)))

;; draw-a-head : circle -> boolean
;; to draw a head
(define (draw-a-head circle)
  (draw-a-noose circle))

;; draw-a-body : body -> boolean
;; to draw a body
(define (draw-a-body body)
  (draw-solid-line (body-start body)
                   (translate-point-y (body-start body)
                                      (body-length body))
                   (body-color body)))
                   
;; draw-a-arm&leg : arm&leg -> boolean
;; to draw a arm & leg
(define (draw-a-arm&leg arm&leg)
  (draw-solid-line (arm&leg-start arm&leg)
                   (translate-point
                    (arm&leg-start arm&leg)
                    (make-posn (* (arm&leg-length arm&leg)
                                  (cos 
                                   (convert-deg2rad (arm&leg-angle arm&leg))))
                               (* (arm&leg-length arm&leg)
                                  (sin 
                                   (convert-deg2rad (arm&leg-angle arm&leg))))))))

;; draw-next-part : symbol -> boolean
;; to draw the matching part of the figure with part
(define (draw-next-part part)
  (cond
    [(symbol=? 'noose part) (draw-a-noose a-noose)]
    [(symbol=? 'head part) (draw-a-head a-head)]
    [(symbol=? 'body part) (draw-a-body a-body)]
    [(symbol=? 'right-arm part) (draw-a-arm&leg right-arm)]
    [(symbol=? 'left-arm part) (draw-a-arm&leg left-arm)]
    [(symbol=? 'right-leg part) (draw-a-arm&leg right-leg)]
    [(symbol=? 'left-leg part) (draw-a-arm&leg left-leg)]))


;; dimensions of canvas
(define CANVAS-WIDTH 200)
(define CANVAS-HEIGHT 200)

;; dimensions of gallows
(define GALLOWS-START-X 0)
(define GALLOWS-START-Y 10)
(define GALLOWS-WIDTH (/ CANVAS-WIDTH 2))
(define GALLOWS-LENGTH 10)
(define GALLOWS-COLOR 'black)

;; the positions of the hangman
(define NOOSE-RADIUS 30)
(define NOOSE-X 120)
(define NOOSE-Y (+ GALLOWS-START-Y GALLOWS-LENGTH 20))
(define NOOSE-COLOR 'red)

(define HANGMAN-COLOR 'black)
(define HEAD-RADIUS 10)
(define HEAD-X (- NOOSE-X (* HEAD-RADIUS 2)))
(define HEAD-Y NOOSE-Y)

(define BODY-X HEAD-X)
(define BODY-Y (+ NOOSE-Y HEAD-RADIUS))
(define BODY-LENGTH 70)

(define ARM-X BODY-X)
(define ARM-Y (+ BODY-Y (* 2 HEAD-RADIUS)))
(define ARM-LENGTH 60)
(define RIGHT-ARM-ANGLE 345)
(define LEFT-ARM-ANGLE 195)

(define LEG-X BODY-X)
(define LEG-Y (+ BODY-Y BODY-LENGTH))
(define LEG-LENGTH 70)
(define RIGHT-LEG-ANGLE 45)
(define LEFT-LEG-ANGLE 135)


(start CANVAS-WIDTH CANVAS-HEIGHT)
(define a-gallows (make-gallows (make-posn GALLOWS-START-X GALLOWS-START-Y)
                                GALLOWS-WIDTH
                                GALLOWS-LENGTH
                                GALLOWS-COLOR))

(define a-noose (make-circle (make-posn NOOSE-X NOOSE-Y)
                             NOOSE-RADIUS
                             NOOSE-COLOR))

(define a-head (make-circle (make-posn HEAD-X HEAD-Y)
                             HEAD-RADIUS
                             HANGMAN-COLOR))

(define a-body (make-body (make-posn BODY-X BODY-Y)
                          BODY-LENGTH
                          HANGMAN-COLOR))

(define right-arm (make-arm&leg (make-posn ARM-X ARM-Y)
                                ARM-LENGTH
                                RIGHT-ARM-ANGLE
                                HANGMAN-COLOR))

(define left-arm (make-arm&leg (make-posn ARM-X ARM-Y)
                               ARM-LENGTH
                               LEFT-ARM-ANGLE
                               HANGMAN-COLOR))

(define right-leg (make-arm&leg (make-posn LEG-X LEG-Y)
                                LEG-LENGTH
                                RIGHT-LEG-ANGLE
                                HANGMAN-COLOR))

(define left-leg (make-arm&leg (make-posn LEG-X LEG-Y)
                               LEG-LENGTH
                               LEFT-LEG-ANGLE
                               HANGMAN-COLOR))

(draw-a-gallows a-gallows)
(draw-next-part 'noose)
(draw-next-part 'head)
(draw-next-part 'body)
(draw-next-part 'right-arm)
(draw-next-part 'left-arm)
(draw-next-part 'right-leg)
(draw-next-part 'left-leg)
