;; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-beginner-reader.ss" "lang")((modname exercise_3_1_2) (read-case-sensitive #t) (teachpacks ((lib "convert.ss" "teachpack" "htdp"))) (htdp-settings #(#t constructor repeating-decimal #f #t none #f ((lib "convert.ss" "teachpack" "htdp")))))
;; profit : number -> nubmer
;; to compute the profit as the difference between revenue and costs
;; at some given ticket-price
;; (profit 5) should produce 415.2
(define (profit ticket-price)
  (- (revenue ticket-price)
     (cost ticket-price)))

;; revennu : number -> number
;; to compute the revenue, given ticket-price
;; (revenue 5) should produce 600
(define (revenue ticket-price)
  (* (attendees ticket-price) ticket-price))

;; cost : number -> number
;; to compute the costs, given ticket-price
;; (cost 5) should produce 184.8
(define (cost ticket-price)
  (+ 180 (* 0.04 (attendees ticket-price))))

;; attendees : number -> number
;; to compute the number of attendees, given ticket-price
;; (attendees 5) should produce 120
;; (attendees 4) should produce 270
;; (attendees 3) should produce 420
(define (attendees ticket-price)
  (+ 120 (* (/ 15 0.1) (- 5 ticket-price))))

