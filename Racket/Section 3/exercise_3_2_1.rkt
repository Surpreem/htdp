;; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-beginner-reader.ss" "lang")((modname exercise_3_2_1) (read-case-sensitive #t) (teachpacks ((lib "convert.ss" "teachpack" "htdp"))) (htdp-settings #(#t constructor repeating-decimal #f #t none #f ((lib "convert.ss" "teachpack" "htdp")))))
;; profit : number -> nubmer
;; to compute the profit as the difference between revenue and costs
;; at some given ticket-price
;; (profit 5) should produce 415.2
(define (profit ticket-price)
  (- (revenue ticket-price)
     (cost ticket-price)))

;; revennu : number -> number
;; to compute the revenue, given ticket-price
;; (revenue 5) should produce 600
(define (revenue ticket-price)
  (* (attendees ticket-price) ticket-price))

;; cost : number -> number
;; to compute the costs, given ticket-price
;; (cost 5) should produce 184.8
(define PERFORMANCE_COST 180)
(define ATTENDEE_COST 0.04)
(define (cost ticket-price)
  (+ PERFORMANCE_COST (* ATTENDEE_COST (attendees ticket-price))))

;; attendees : number -> number
;; to compute the number of attendees, given ticket-price
;; (attendees 5) should produce 120
;; (attendees 4) should produce 270
;; (attendees 3) should produce 420
(define ATTENDEE_BASE 120)
(define PRICE_BASE 5)
(define ATTENDEE_PER_DIME 15)
(define DIME 0.1)
(define (attendees ticket-price)
  (+ ATTENDEE_BASE (* (/ ATTENDEE_PER_DIME DIME) (- PRICE_BASE ticket-price))))

;;
(define (profit2 price)
  (- (* (+ ATTENDEE_BASE
	   (* (/ ATTENDEE_PER_DIME DIME)
	      (- PRICE_BASE price)))
	price)
     (+ PERFORMANCE_COST 
	(* ATTENDEE_COST
	   (+ ATTENDEE_BASE
	      (* (/ ATTENDEE_PER_DIME DIME)
		 (- PRICE_BASE price)))))))