;; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-beginner-reader.ss" "lang")((modname exercise_7_5_2_e) (read-case-sensitive #t) (teachpacks ((lib "draw.ss" "teachpack" "htdp"))) (htdp-settings #(#t constructor repeating-decimal #f #t none #f ((lib "draw.ss" "teachpack" "htdp")))))
;; Data Definitions:
(define-struct circle (center radius))
;; A circle is a structure:
;;          (make-circle p s)
;;    where p is a posn, s a number;

(define-struct square (nw length))
;; A square is a structure:
;;          (make-square p s)
;;    where p is a posn, s a number.

;; A shape is either
;; 1. a circle, or
;; 2. a square.

;; checked-perimeter : Scheme-value -> nubmer
;; to compute the perimeter of a-shape
;; if a-shape is a circle or a square.
(define (checked-perimeter a-shape)
  (cond
    [(or (circle? a-shape) (square? a-shape)) (perimeter a-shape)]
    [else (error 'checked-perimeter "circle or square expected")]))

;; Final Definitions:
;; perimeter : shape  ->  number
;; to compute the perimeter of a-shape
(define (perimeter a-shape)
  (cond
    [(circle? a-shape)
     (perimeter-circle a-shape)]
    [(square? a-shape)
     (perimeter-square a-shape)]))

;; perimeter-circle : circle  ->  number
;; to compute the perimeter of a-circle
(define (perimeter-circle a-circle)
  (* (* 2 (circle-radius a-circle)) pi))

;; perimeter-square : square  ->  number
;; to compute the perimeter of a-square
(define (perimeter-square a-square)
  (* (square-length a-square) 4))

;; Test expressions
(define a-circle (make-circle (make-posn 3 4) 5))
(define a-square (make-square (make-posn 5 5) 4))

(checked-perimeter a-circle)
(checked-perimeter a-square)
(checked-perimeter (make-posn 3 4))
