;; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-beginner-reader.ss" "lang")((modname exercise_4_2_2) (read-case-sensitive #t) (teachpacks ((lib "convert.ss" "teachpack" "htdp"))) (htdp-settings #(#t constructor repeating-decimal #f #t none #f ((lib "convert.ss" "teachpack" "htdp")))))
;; the interval is (-3, 0)
;; in-interval-1? : number -> boolean
;; to determine whether x is greater than -3 and less than 0
(define (in-interval-1? x)
  (and (< -3 x) (< x 0)))

;; the interval is outside [1, 2]
;; in-interval-2? : number -> boolean
;; to determine whether x is less than 1 or greater than 2
(define (in-interval-2? x)
  (or (< x 1) (> x 2)))

;; the interval is outside [1, 5]
;; in-interval-3? : number -> boolena
;; to determine whether x is less than 1 or greater than 5
(define (in-interval-3? x)
  (not (and (<= 1 x) (<= x 5))))
