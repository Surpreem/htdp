;; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-beginner-reader.ss" "lang")((modname exercise_4_4_2) (read-case-sensitive #t) (teachpacks ((lib "convert.ss" "teachpack" "htdp"))) (htdp-settings #(#t constructor repeating-decimal #f #t none #f ((lib "convert.ss" "teachpack" "htdp")))))
;; tax-rate : number -> number
;; to compute the tax rate for the given gross >= 0
(define (tax-rate gross)
  (cond
    [(<= gross 240) 0.0]
    [(<= gross 480) 0.15]
    [(< 480 gross) 0.28]))

;; test expressions
(= (tax-rate 240) 0.0)
(= (tax-rate 480) 0.15)
(= (tax-rate 481) 0.28)


;; tax : number -> number
;; to compute the tax for the given gross >= 0
(define (tax gross)
  (* gross (tax-rate gross)))

;; test exprerssions
(= (tax 240) 0)
(= (tax 480) 72)
(= (tax 481) 134.68)


(define PAY_PER_HOUR 12)

;; grosspay : number -> number
;; to compute the gross pay for the given hours worked
(define (grosspay hours)
  (* hours PAY_PER_HOUR))


;; netpay : number -> number
;; to compute net pay for the given hours worked
(define (netpay hours)
  (- (grosspay hours) (tax (grosspay hours))))

;; test expression
(= (netpay 3) 36)
(= (netpay 30) 306)
(= (netpay 48) 414.72)
